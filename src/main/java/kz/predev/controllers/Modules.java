package kz.predev.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class Modules {
	
	@GetMapping("/module1")
	public String module1(Model model) {
		
		return "pages/labs/day1";
		
	}
	@GetMapping("/module2")
	public String module2(Model model) {
		
		return "pages/labs/day2";
		
	}
	@GetMapping("/module3")
	public String module3(Model model) {
		
		return "pages/labs/day3";
		
	}
	@GetMapping("/module4")
	public String module4(Model model) {
		
		return "pages/labs/day4";
		
	}
	@GetMapping("/module5")
	public String module5(Model model) {
		
		return "pages/labs/day5";
		
	}
	
	@GetMapping("/day1/split")
	public String day1split(Model model) {
		
		return "/layout/navpages/labs/day_1/split";
		
	}

}
