package kz.predev.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PageController  {
	
	@GetMapping("/")
	public String index(Model model) {
		
		return "pages/index";
		
	}
	
	@GetMapping("/videocourse")
	public String videoCourse(Model model) {
		
		return "pages/videocourse";
		
	}
	
	@GetMapping("/login")
	public String customLogin(Model model) {
		
		return "pages/pages-login";
		
	}
	
	@GetMapping("/linux-essentials")
	public String linuxEssentials(Model model) {
		
		return "pages/linux-essentials/bandit";
		
	}
	
	@GetMapping("/linux-essentials/bandit")
	public String bandit(Model model) {
				
		return "/layout/navpages/linux-essentials/split";
		
	}
	
	

}
