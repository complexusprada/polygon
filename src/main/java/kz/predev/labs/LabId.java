package kz.predev.labs;

public class LabId {
	
	
	private int day;
	private int labId;
	
	public int getDay() {
		return day;
	}
	
	public void setDay(int day) {
		this.day = day;
	}
	
	public int getLabId() {
		return labId;
	}
	
	public void setLabId(int labid) {
		this.labId = labid;
	}
	
	
}
