package kz.predev.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.socket.server.standard.ServerEndpointExporter;

import kz.predev.guacamole.WebSocketTunnel;

@Configuration	
@EnableAsync
public class WebSocketConfig {
	
	@Bean
	  public WebSocketTunnel chatEndpoint(){
	     return new WebSocketTunnel();
	  }

	@Bean
	public ServerEndpointExporter serverEndpointExporter() {
		
		return new ServerEndpointExporter();
	}
}
