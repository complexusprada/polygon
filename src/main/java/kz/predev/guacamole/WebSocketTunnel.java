package kz.predev.guacamole;


import javax.websocket.EndpointConfig;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;

import org.apache.guacamole.GuacamoleException;
import org.apache.guacamole.net.GuacamoleSocket;
import org.apache.guacamole.net.GuacamoleTunnel;
import org.apache.guacamole.net.InetGuacamoleSocket;
import org.apache.guacamole.net.SimpleGuacamoleTunnel;
import org.apache.guacamole.protocol.ConfiguredGuacamoleSocket;
import org.apache.guacamole.protocol.GuacamoleClientInformation;
import org.apache.guacamole.protocol.GuacamoleConfiguration;
import org.apache.guacamole.websocket.GuacamoleWebSocketTunnelEndpoint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import kz.predev.SpringContext;
import kz.predev.database.UserRepository;
import kz.predev.display.DimensionHandler;
import kz.predev.vcenter.labs.machines.CredentialsConfigurer;


@ServerEndpoint(value = "/day1/webSocket",subprotocols = "guacamole")
@Component
public class WebSocketTunnel extends GuacamoleWebSocketTunnelEndpoint {
	
	@Autowired
	UserRepository userRepository = SpringContext.getBean(UserRepository.class); 
	
	@Override	
	protected GuacamoleTunnel createTunnel(Session session, EndpointConfig endpointConfig) throws GuacamoleException {

		int port = 4822;
		
		String guacd = this.getGuacIP(session);
		
		DimensionHandler dHandler = new DimensionHandler(session);
		GuacamoleClientInformation client = new GuacamoleClientInformation();

		
		int clientHeight = (int) Double.parseDouble(dHandler.getHeight());
		int clientWidth = (int) Double.parseDouble(dHandler.getWidth());
		
		client.setOptimalScreenHeight(clientHeight);
		client.setOptimalScreenWidth(clientWidth);


		if(dHandler.getVmName().equals("BANDIT")){
			
			guacd = System.getenv("GUAC_IP");
		}
	
		
		GuacamoleConfiguration configuration = new CredentialsConfigurer(session).getConfiguration();
		
		GuacamoleSocket socket = new ConfiguredGuacamoleSocket( new InetGuacamoleSocket(guacd, port), configuration,client);
	
		GuacamoleTunnel tunnel = new SimpleGuacamoleTunnel(socket);
					
		return tunnel;

	}
	
	
	public String getGuacIP(Session session) {
		
		String username = session.getUserPrincipal().getName();
		
		String guacip = userRepository.findByUserName(username).getGateway().getGuacIP();
		
		return guacip;
		
	}
	

	
}