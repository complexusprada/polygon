package kz.predev.vcenter.api;

import java.security.KeyStore;
import java.util.Map;


import com.vmware.vapi.bindings.StubConfiguration;
import com.vmware.vapi.protocol.HttpConfiguration;
import com.vmware.vapi.protocol.HttpConfiguration.KeyStoreConfig;
import com.vmware.vapi.protocol.HttpConfiguration.SslConfiguration;


/**
 * Abstract base class for vSphere samples
 *
 */
public abstract class SamplesAbstractBase {
    public String server;
    public String username;
    public String password;
    public boolean clearData;
    public boolean skipServerVerification;
    public String truststorePath;
    public String truststorePassword;
    public String configFile;
    public VimAuthenticationHelper vimAuthHelper;
    public VapiAuthenticationHelper vapiAuthHelper;
    public StubConfiguration sessionStubConfig;
    public Map<String, Object> parsedOptions;

    /**
     * Parses the command line arguments / config file and creates a map of
     * key-value pairs having all the required options for the sample
     *
     * @param sampleOptions List of options required by the sample
     * @param args String array of arguments
     */
    

    public String getServer() {
        return this.server;
    }

    public String getUsername() {
        return this.username;
    }

    public String getPassword() {
        return this.password;
    }

    public boolean isClearData() {
        return clearData;
    }

    public boolean isSkipServerVerification() {
        return skipServerVerification;
    }

    public String getTruststorePath() {
        return truststorePath;
    }

    public String getTruststorePassword() {
        return truststorePassword;
    }

    public String getConfigFile() {
        return configFile;
    }

  


    /**
     * Setup any resources required for the sample run.
     * @throws Exception
     */
    public abstract void setup() throws Exception;

    /**
     * Run the sample
     * @throws Exception
     */
    public abstract void run() throws Exception;

    /**
     * Clean up any sample data created as part of the sample run.
     * @throws Exception
     */
    public abstract void cleanup() throws Exception;

    /**
     * Creates a session with the server using username/password.
     *
     *<p><b>
     * Note: If the "skip-server-verification" option is specified, then this
     * method trusts the SSL certificate from the server and doesn't verify
     * it. Circumventing SSL trust in this manner is unsafe and should not be
     * used with production code. This is ONLY FOR THE PURPOSE OF DEVELOPMENT
     * ENVIRONMENT
     * <b></p>
     * @throws Exception
     */
    public void login() throws Exception {
        this.vapiAuthHelper = new VapiAuthenticationHelper();
        this.vimAuthHelper = new VimAuthenticationHelper();
        HttpConfiguration httpConfig = buildHttpConfiguration();
        this.sessionStubConfig =
                vapiAuthHelper.loginByUsernameAndPassword(
                    this.server, this.username, this.password, httpConfig);
        this.vimAuthHelper.loginByUsernameAndPassword(
                    this.server, this.username, this.password);
    }

    /**
     * Builds the Http settings to be applied for the connection to the server.
     * @return http configuration
     * @throws Exception 
     */
    protected HttpConfiguration buildHttpConfiguration() throws Exception {
        HttpConfiguration httpConfig =
            new HttpConfiguration.Builder()
            .setSslConfiguration(buildSslConfiguration())
            .getConfig();
        
        return httpConfig;
	}

	/**
     * Builds the SSL configuration to be applied for the connection to the
     * server
     * 
     * For vApi connections:
     * If "skip-server-verification" is specified, then the server certificate
     * verification is skipped. The method retrieves the certificate
     * from specified server and adds it to an in-memory trustStore which is
     * returned.
     * If "skip-server-verification" is not specified, then it uses the
     * truststorepath and truststorepassword to load the truststore and return
     * it.
     *
     * For VIM connections:
     * If "skip-server-verification" is specified, then it trusts all the
     * VIM API connections made to the specified server.
     * If "skip-server-verification" is not specified, then it sets the System
     * environment property "javax.net.ssl.trustStore" to the path of the file
     * containing the trusted server certificates.
     *
     *<p><b>
     * Note: Below code circumvents SSL trust if "skip-server-verification" is
     * specified. Circumventing SSL trust is unsafe and should not be used
     * in production software. It is ONLY FOR THE PURPOSE OF DEVELOPMENT
     * ENVIRONMENTS.
     *<b></p>
     * @return SSL configuration
     * @throws Exception
     */
    protected SslConfiguration buildSslConfiguration() throws Exception {
        SslConfiguration sslConfig;

        if(this.skipServerVerification) {
            /*
             * Below method enables all VIM API connections to the server
             * without validating the server certificates.
             *
             * Note: Below code is to be used ONLY IN DEVELOPMENT ENVIRONMENTS.
             * Circumventing SSL trust is unsafe and should not be used in
             * production software.
             */
            SslUtil.trustAllHttpsCertificates();

            /*
             * Below code enables all vAPI connections to the server
             * without validating the server certificates..
             *
             * Note: Below code is to be used ONLY IN DEVELOPMENT ENVIRONMENTS.
             * Circumventing SSL trust is unsafe and should not be used in
             * production software.
             */
            sslConfig = new SslConfiguration.Builder()
            		.disableCertificateValidation()
            		.disableHostnameVerification()
            		.getConfig();
        } else {
            /*
             * Set the system property "javax.net.ssl.trustStore" to
             * the truststorePath
             */
            System.setProperty("javax.net.ssl.trustStore", this.truststorePath);
            KeyStore trustStore =
                SslUtil.loadTrustStore(this.truststorePath,
                		this.truststorePassword);
            KeyStoreConfig keyStoreConfig =
            		new KeyStoreConfig("", this.truststorePassword);
            sslConfig =
            		new SslConfiguration.Builder()
            		.setKeyStore(trustStore)
            		.setKeyStoreConfig(keyStoreConfig)
            		.getConfig();
        }

        return sslConfig;
    }

    /**
     * Logs out of the server
     * @throws Exception
     */
    public void logout() throws Exception {
        this.vapiAuthHelper.logout();
        this.vimAuthHelper.logout();
    }

    /**
     * Executes the sample using the command line arguments or parameters from
     * the configuration file. Execution involves the following steps:
     * 1. Parse the arguments required by the sample
     * 2. Login to the server
     * 3. Setup any resources required by the sample run
     * 4. Run the sample
     * 5. Cleanup any data created by the sample run, if cleanup=true
     * 6. Logout of the server
     *
     * @param args command line arguments passed to the sample
     * @throws Exception
     */
    protected void execute() throws Exception {
        try {

            // Login to the server
            login();

            // Setup any resources required by the sample
            setup();

            // Execute the sample
            run();

            if (clearData) {
                // Clean up the sample data
                cleanup();
            }

        } finally {
            // Logout of the server
            logout();
        }
    }
}

