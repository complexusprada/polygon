/*
 * *******************************************************
 * Copyright VMware, Inc. 2017.  All Rights Reserved.
 * SPDX-License-Identifier: MIT
 * *******************************************************
 *
 * DISCLAIMER. THIS PROGRAM IS PROVIDED TO YOU "AS IS" WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, WHETHER ORAL OR WRITTEN,
 * EXPRESS OR IMPLIED. THE AUTHOR SPECIFICALLY DISCLAIMS ANY IMPLIED
 * WARRANTIES OR CONDITIONS OF MERCHANTABILITY, SATISFACTORY QUALITY,
 * NON-INFRINGEMENT AND FITNESS FOR A PARTICULAR PURPOSE.
 */
package kz.predev.vcenter.api;

import java.util.Collections;
import java.util.List;

import com.vmware.vcenter.VM;
import com.vmware.vcenter.VMTypes.Summary;
import com.vmware.vcenter.VMTypes.FilterSpec.Builder;

import kz.predev.vcenter.api.helpers.ClusterHelper;
import kz.predev.vcenter.api.helpers.DatacenterHelper;
import kz.predev.vcenter.api.helpers.FolderHelper;

/**
 * Description: Demonstrates getting list of VMs present in vCenter
 *
 * Author: VMware, Inc.
 * Sample Prerequisites: vCenter 6.5+
 */
public class ListVMs extends SamplesAbstractBase {
    private VM vmService;
    public String vmFolderName;
    public String datacenterName;
    public String clusterName;

    /**
     * Define the options specific to this sample and configure the sample using
     * command-line arguments or a config file
     *
     * @param args command line arguments passed to the sample
     */
    
    
    public void setup() throws Exception {
    	
        this.vmService =
                vapiAuthHelper.getStubFactory()
                    .createStub(VM.class, sessionStubConfig);
    }

    public void run() throws Exception {
        Builder bldr = new Builder();
        if(null != this.datacenterName && !this.datacenterName.isEmpty()){
            bldr.setDatacenters(Collections.singleton(DatacenterHelper.
                  getDatacenter(this.vapiAuthHelper.getStubFactory(),
                          this.sessionStubConfig, this.datacenterName)));
        }
        if(null != this.clusterName && !this.clusterName.isEmpty()) {
            bldr.setClusters(Collections.singleton(ClusterHelper.getCluster(
                  this.vapiAuthHelper.getStubFactory(), sessionStubConfig,
                  this.clusterName)));
        }      
        if(null != this.vmFolderName && !this.vmFolderName.isEmpty())
        {
            bldr.setFolders(Collections.singleton(FolderHelper.getFolder(
                  this.vapiAuthHelper.getStubFactory(), sessionStubConfig,
                  this.vmFolderName)));
        }
        List<Summary> vmList = this.vmService.list(bldr.build());
        
        System.out.println("----------------------------------------");
        System.out.println("List of VMs");
        
        for (Summary vmSummary : vmList) {
            System.out.println(vmSummary);
        }
        
        System.out.println("----------------------------------------");
    }
    
    public List<Summary> getMachines(){
    	
    	Builder bldr = new Builder();
        if(null != this.datacenterName && !this.datacenterName.isEmpty()){
            bldr.setDatacenters(Collections.singleton(DatacenterHelper.
                  getDatacenter(this.vapiAuthHelper.getStubFactory(),
                          this.sessionStubConfig, this.datacenterName)));
        }
        if(null != this.clusterName && !this.clusterName.isEmpty()) {
            bldr.setClusters(Collections.singleton(ClusterHelper.getCluster(
                  this.vapiAuthHelper.getStubFactory(), sessionStubConfig,
                  this.clusterName)));
        }      
        if(null != this.vmFolderName && !this.vmFolderName.isEmpty())
        {
            bldr.setFolders(Collections.singleton(FolderHelper.getFolder(
                  this.vapiAuthHelper.getStubFactory(), sessionStubConfig,
                  this.vmFolderName)));
        }
        List<Summary> vmList = this.vmService.list(bldr.build());
        
        return vmList;
    	
    	
    }
    
    public void cleanup() throws Exception {
    	// No cleanup required
    }

	
}
