package kz.predev.vcenter.labs.machines;

import javax.websocket.Session;

import org.apache.guacamole.protocol.GuacamoleClientInformation;
import org.apache.guacamole.protocol.GuacamoleConfiguration;

import kz.predev.display.DimensionHandler;


public class CredentialsConfigurer {
	
	private Session session;
	
	public  CredentialsConfigurer(Session session) {
		
		this.session = session;
		
	}
	
	public GuacamoleConfiguration getConfiguration() {
		
		GuacamoleConfiguration configuration = new GuacamoleConfiguration();
		DimensionHandler dHandler = new DimensionHandler(session);
		
		if(dHandler.getVmName().equals("UBUNTU03") ) {
			
			configuration.setProtocol("ssh");
			configuration.setParameter("hostname", dHandler.getVmIp());
			configuration.setParameter("port", "22");
			configuration.setParameter("username", this.getUserName(dHandler.getVmName()));
			configuration.setParameter("password", this.getPassword(dHandler.getVmName()));
			configuration.setParameter("width", dHandler.getWidth());
			configuration.setParameter("height", dHandler.getHeight());
			//configuration.setParameter("resize-method", "display-update");
			
			return configuration;
			
		}
		
		if(dHandler.getVmName().equals("BANDIT") ) {
			
			configuration.setProtocol("ssh");
			configuration.setParameter("hostname", dHandler.getVmIp());
			configuration.setParameter("port", "2220");
			configuration.setParameter("username", this.getUserName(dHandler.getVmName()));
			configuration.setParameter("password", this.getPassword(dHandler.getVmName()));
			
			
			return configuration;
			
		}
		
		configuration.setProtocol("rdp");
		configuration.setParameter("hostname", dHandler.getVmIp());
		configuration.setParameter("port", "3389");
		//configuration.setParameter("username", this.getUserName(dHandler.getVmName()));
		//configuration.setParameter("password", this.getPassword(dHandler.getVmName()));
		configuration.setParameter("ignore-cert", "true");
		configuration.setParameter("enable-wallpaper", "true");
		
		configuration.setParameter("width", dHandler.getWidth());
		configuration.setParameter("height", dHandler.getHeight());
		configuration.setParameter("resize-method", "display-update");
		
		if(dHandler.getVmName().contains("OUT")) {
			
			configuration.unsetParameter("username");
			configuration.unsetParameter("password");
			
		}
		
		
		return configuration;

	}
	

	public String getUserName(String vmName) {
		
			switch (vmName) {
			
				case "KALI":
					return "student";
				case "WINDOWS01":
					return "dwight.schrute";
				case "WINDOWS02":
					return "alan.marshall";
				case "DC":
					return "Administrator";
				case "UBUNTU01":
					return "alanmarshall";
				case "UBUNTU02":
					return "alanmarshall";
				case "UBUNTU03":
					return "alanmarshall";
				case "BANDIT":
					return "bandit0";
			}
	
		
		return "student";
			
		
	}
	
	
	public String getPassword(String vmName) {
		
		switch (vmName) {
		
			case "KALI":
				return "Awesomesauce123";
			case "WINDOWS01":
				return "BattleSt4r";
			case "WINDOWS02":
				return "Awesomesauce123";
			case "DC":
				return "Kazl4bs";
			case "UBUNTU01":
				return "Awesomesauce123";
			case "UBUNTU02":
				return "Awesomesauce123";
			case "UBUNTU03":
				return "Awesomesauce123";
			case "BANDIT":
				return "bandit0";
			
		}
		
		return "Awesomesauce123";
		
	}

}
