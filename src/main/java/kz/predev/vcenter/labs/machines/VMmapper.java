package kz.predev.vcenter.labs.machines;

public class VMmapper {
	
	
	public static String map(String vmName) {
		
		if(vmName.contains("OUT")) {
			
			vmName = vmName.split("_")[1];
			
		}
		

		switch (vmName) {
			
			case "KALI":
				return "192.168.10.39";
			case "WINDOWS01":
				return "192.168.10.15";
			case "WINDOWS02":
				return "192.168.10.16";
			case "DC":
				return "192.168.10.5";
			case "UBUNTU01":
				return "192.168.20.10";
			case "UBUNTU02":
				return "192.168.30.15";
			case "UBUNTU03":
				return "192.168.30.16";
			case "BANDIT":
				return "176.9.9.172";
				
		}
		
		return "192.168.10.39";

	}

}
