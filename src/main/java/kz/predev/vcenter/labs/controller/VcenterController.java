package kz.predev.vcenter.labs.controller;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import kz.predev.database.services.UserService;
import kz.predev.labs.LabId;
import kz.predev.vcenter.labs.services.VcenterService;

@Controller
@RequestMapping(path="/iclone/v1")
public class VcenterController {
	
	@Autowired
	private VcenterService vcenterService;
	
	@Autowired
	private UserService userService;
	
	@PostMapping(value="/create")
	public ResponseEntity<?> startIClone(@RequestBody LabId labId) throws Exception {
		
		vcenterService.cloneHandler(userService.getUserId(),labId.getLabId(),0);
		
		return ResponseEntity.status(HttpStatus.CREATED).build();

		
	}
	
	@PostMapping(value="/switch")
	public ResponseEntity<?> switchLab(@RequestBody LabId labId) throws Exception {
		
		
		vcenterService.switchMachines(userService.getUserId(),labId.getLabId());
		
		return ResponseEntity.status(HttpStatus.CREATED).build();

		
	}
	
	@GetMapping(value="/correct")
	public ResponseEntity<?> checkDatabase(@RequestParam(name = "active") boolean active) throws Exception {
		
		if(active == true) {
			
			vcenterService.correctDbValue(userService.getUserId());

		}else {
			
			vcenterService.correctActiveLabs(userService.getUserId());
		}
		
		
		return ResponseEntity.status(HttpStatus.ACCEPTED).build();

	}
	
	
	@PostMapping(value="/destroy")
	public ResponseEntity<?> deleteClones(@RequestBody LabId labId) throws Exception {
			
		vcenterService.cloneHandler(userService.getUserId(),labId.getLabId(),1);
		
		return ResponseEntity.status(HttpStatus.PROCESSING).build();
		
	}
	
	/*
	 * @GetMapping(value="/test") public ResponseEntity Test() throws Exception {
	 * 
	 * System.out.println(userService.getUserId());
	 * 
	 * return ResponseEntity.status(HttpStatus.OK).build();
	 * 
	 * 
	 * 
	 * }
	 */

}
