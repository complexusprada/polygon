package kz.predev.vcenter.labs.services;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import kz.predev.database.labs.CurrentLabRepository;
import kz.predev.database.services.UserService;
import kz.predev.vcenter.labs.checker.DoubleCheck;
import kz.predev.vcenter.labs.managers.VMmanager;

@Service
public class VcenterService {
	
	@Autowired
	UserService userService;
	
	@Autowired
	private CurrentLabRepository currentLabRepo;
	
	@Async("threadPoolTaskExecutor")
	public void cloneHandler(int studentId, int labId ,int destroyFlag) throws Exception {
		
		new VMmanager(userService, studentId,currentLabRepo).manageMachines(labId, destroyFlag);
		
	}
	
	@Async("threadPoolTaskExecutor")
	public void correctDbValue(int studentId) throws Exception {
		
		new DoubleCheck(userService, studentId,currentLabRepo).correctDbValue();
		
	}
	
	@Async("threadPoolTaskExecutor")
	public void correctActiveLabs(int studentId) throws Exception {
		
		new DoubleCheck(userService, studentId,currentLabRepo).correctActiveMachines();
		
	}
	
	@Async("threadPoolTaskExecutor")
	public void switchMachines(int studentId, int labid) throws Exception {
		
		new VMmanager(userService, studentId,currentLabRepo).switchLaboratory(labid);
		
	}

}
