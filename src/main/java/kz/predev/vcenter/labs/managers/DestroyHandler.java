package kz.predev.vcenter.labs.managers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.session.SessionDestroyedEvent;
import org.springframework.stereotype.Component;

import kz.predev.database.PolygonUserPrincipal;
import kz.predev.database.User;
import kz.predev.database.labs.CurrentLab;
import kz.predev.database.labs.CurrentLabRepository;
import kz.predev.database.services.MyUserDetailsService;
import kz.predev.database.services.UserService;
import kz.predev.vcenter.labs.services.VcenterService;

@Component
public class DestroyHandler implements ApplicationListener<SessionDestroyedEvent> {
	
	@Autowired
	VcenterService vcenterService;
	
	@Autowired
	CurrentLabRepository currentLabRepo;
	
	@Override
    public void onApplicationEvent(SessionDestroyedEvent event) {
     	
			
        for (SecurityContext securityContext : event.getSecurityContexts()){
        	
            Authentication authentication = securityContext.getAuthentication();
            PolygonUserPrincipal userDetails = (PolygonUserPrincipal) authentication.getPrincipal();
            User user = userDetails.getUser();
            
            
            
            try {
            	
      		  	CurrentLab currentLab = new CurrentLab();

            	vcenterService.cloneHandler(user.getId(), 0 , 1);
            	
            	currentLab.setCurrentLabId(0);
      		  	currentLab.setId(user.getId());
      			
      		  	currentLabRepo.save(currentLab);
					
			} catch (Exception e) {
				
				// TODO Auto-generated catch block
				e.printStackTrace();
				
			}
            
        }
    }

	

}
