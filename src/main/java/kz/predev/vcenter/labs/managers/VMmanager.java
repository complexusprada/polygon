package kz.predev.vcenter.labs.managers;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import kz.predev.database.labs.CurrentLab;
import kz.predev.database.labs.CurrentLabRepository;
import kz.predev.database.labs.Gateway;
import kz.predev.database.services.UserService;


public class VMmanager {
	
	private String guacip;
	private UserService userService;
	private CurrentLabRepository currentLabRepo;
	private int studentId;

	
	
	public VMmanager(UserService userService, int studentId, CurrentLabRepository currentLabRepo) {
		
		this.userService = userService;
		this.studentId = studentId;
		this.currentLabRepo = currentLabRepo;
		
	}
	
	
	public void switchLaboratory(int labId) throws Exception {
		
		 
		  if(protectedExecution(1).equals("CLOSED")) {
			  
			  return;
			  
		  }
		  
		  System.out.println("hello");
		  setGuacdIp(userService, false, "0.0.0.0");
		  
		  Runtime r = Runtime.getRuntime();
		  
		  Process p = r.exec("/usr/bin/bash /root/MVP/switchlab.sh "+ studentId + " 0 " + labId);
		  
		  p.waitFor();
		  
		  BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
		  
		  String guacip = b.readLine().replaceAll("\\s+","");
		  
		  b.close();
		  
		  p.destroyForcibly();
		  
		  setGuacdIp(userService, true, guacip);
		  
		  updateCurrentLab(labId);
		  
		  
		  
	}

	
	public void manageMachines(int labId, int destroyFlag) throws Exception {
		
		  if(protectedExecution(0).equals("CLOSED")) {
			  
			  return;
			  
		  }
		  
		  Runtime r = Runtime.getRuntime();
		  Process p;
		  		  
		  if( destroyFlag == 1 ) {
			  
			  p = r.exec("/usr/bin/bash /root/MVP/getip.sh "+ studentId + " " + labId + " " + destroyFlag);
			  
		  }else {
			  
			  p = r.exec("/usr/bin/bash /root/MVP/getip.sh "+ studentId + " " + labId);

		  }
		  
		  p.waitFor();
		  
		  BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
		  
		  guacip = b.readLine().replaceAll("\\s+","");
		  
		  b.close();
		  
		  p.destroyForcibly();
		  
		  if ( destroyFlag == 1 ) {
			  
			  guacip = "0.0.0.0";
					  
			  setGuacdIp(userService, false, guacip);
			  
			  updateCurrentLab(0);
			  
			  return;
			  
		  }else {
			  
			  setGuacdIp(userService, true, guacip);
			  
			  updateCurrentLab(labId);
			  
		  }
		  
	}
	
	
	public void setGuacdIp(UserService userService, boolean isActive, String guacip) {
		
		  Gateway gateway = new Gateway();
		  
		  gateway.setId(studentId);
		  
		  gateway.setGuacIP(guacip);
		  
		  gateway.setActive(isActive);
			 		  
		  userService.saveGateway(gateway);	 
		
	}
	
	public void updateCurrentLab(int labId) {
		
		  CurrentLab currentLab = new CurrentLab();

		  currentLab.setCurrentLabId(labId);
		  currentLab.setId(studentId);
			
		  currentLabRepo.save(currentLab);
		
	}
	
	
	
	public String protectedExecution(int safeExId) throws Exception {
		
		 Runtime r = Runtime.getRuntime();
		 
		 String scriptName = "check.sh";
		 
		 if( safeExId == 0) {
			 
			 scriptName = "check.sh";
			 
		 }else if( safeExId == 1) {
			 
			 scriptName = "safe-switch.sh";
			 
		 }
		 
		 Process p = r.exec("/usr/bin/bash /root/MVP/"+ scriptName + " " + studentId);
		 
		 p.waitFor();
		  
		 BufferedReader b = new BufferedReader(new InputStreamReader(p.getInputStream()));
		 
		 String status = b.readLine().replaceAll("\\s+","");
		 
		 b.close();
		  
		 p.destroyForcibly();
		 
		 return status;
		
	}
	
}
