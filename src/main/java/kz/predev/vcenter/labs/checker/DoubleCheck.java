package kz.predev.vcenter.labs.checker;

import com.vmware.vcenter.VMTypes.Summary;

import kz.predev.database.labs.CurrentLabRepository;
import kz.predev.database.labs.Gateway;
import kz.predev.database.services.UserService;
import kz.predev.vcenter.api.ListVMs;
import kz.predev.vcenter.labs.managers.VMmanager;

public class DoubleCheck {
	
	private UserService userService;
	private CurrentLabRepository currentLabRepo;

	int studentId;
	
	public DoubleCheck(UserService userService, int studentId,CurrentLabRepository currentLabRepo) {
		
		this.userService = userService;
		this.studentId = studentId;
		this.currentLabRepo = currentLabRepo;
		
	}
	
	public void unsetDbValue() {
		
		  Gateway gateway = new Gateway();
		  		  
		  gateway.setId(studentId);
		  
		  gateway.setGuacIP("0.0.0.0");
		  
		  gateway.setActive(false);
			 		  
		  userService.saveGateway(gateway);	 
		
	}
	
	
	public boolean isMachineActive() throws Exception {
		
		
		ListVMs list = new ListVMs();
		
        list.server="10.0.10.3";
        list.clusterName="Polygon";
		list.username = "administrator@vsphere.local";
		list.password = "KJHCs86clnkj6&Y%&^";
		list.skipServerVerification = true;
		
		list.login();
		
		list.setup();
		
        for (Summary vmSummary : list.getMachines()) {
        	
            if(vmSummary.getName().equals("student"+studentId+"-guacd")) {
            	
                list.logout();

            	return true;
            	
            }
        }

        list.logout();

        return false;
        
	}
	

	
	public void correctActiveMachines() throws Exception {
		
		if(isMachineActive() == true) {
			
			VMmanager vmManager = new VMmanager(userService, studentId,currentLabRepo);
			vmManager.manageMachines(0, 1);
			vmManager.updateCurrentLab(0);
			
			return;
		}
		
		
	}
	
	
	public void correctDbValue() throws Exception {
		
		if(isMachineActive() == false) {
			
	        this.unsetDbValue();
	        
			VMmanager vmManager = new VMmanager(userService, studentId,currentLabRepo);
			vmManager.updateCurrentLab(0);
		}
        
	}

}
