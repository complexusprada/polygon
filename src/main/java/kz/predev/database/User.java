package kz.predev.database;

import javax.persistence.Table;

import kz.predev.database.labs.CurrentLab;
import kz.predev.database.labs.Gateway;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinTable;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

@Entity
@Table(name="users")
public class User {
	
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  @Column(name = "user_id")
  private int id;
  
  @Column(name = "login")
  private String userName;
  
  @Column(name = "password")
  private String password;

  @Column(name = "active")
  private Boolean active;
  
	  
  @ManyToMany(cascade = CascadeType.MERGE)  
  @JoinTable(name = "user_roles", joinColumns = @JoinColumn(name = "user_id"), inverseJoinColumns = @JoinColumn(name = "role_id")) 
  private Set<Role> student_roles;
  
  
  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "user_id", referencedColumnName = "student_id")
  private Gateway gateway;
  
  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "user_id", referencedColumnName = "student_id")
  private CurrentLab currentLab;
	 
  
	public Gateway getGateway() {
		return gateway;
	}

	public void setGateway(Gateway gateway) {
		this.gateway = gateway;
	}

	public Integer getId() {
		return id;
	}
	
	public void setId(Integer id) {
		this.id = id;
	}
	
	public String getLogin() {
		return userName;
	}
	
	public void setLogin(String user_name) {
		this.userName = user_name;
	}
	
	public String getPassword() {
		return password;
	}
	
	public void setPassword(String password) {
		this.password = password;
	}
	
	public Boolean isActive() {
		return active;
	}
	
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	
	public Set<Role> getRoles() { 
		
		return student_roles; 
	}
	  
	
	public void setRoles(Set<Role> roles) { 
		  
		  this.student_roles = roles; 	  
	}
	
}
