package kz.predev.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import kz.predev.database.RoleRepository;
import kz.predev.database.User;
import kz.predev.database.UserRepository;
import kz.predev.database.labs.Gateway;
import kz.predev.database.labs.GatewayRepository;



@Service
public class UserService {
	
	@Autowired
    private UserRepository userRepository;
	
	@Autowired
    private RoleRepository roleRepository;
	
	@Autowired
    private GatewayRepository gatewayRepository;
    
    @Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;

    @Autowired
    public UserService(UserRepository userRepository, RoleRepository roleRepository, BCryptPasswordEncoder bCryptPasswordEncoder, GatewayRepository lab1Repository) {
        
    	this.userRepository = userRepository;
        this.roleRepository = roleRepository;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.gatewayRepository = lab1Repository;
               
    }
    
    public User findUserById(int id) {
        return userRepository.findById(id);
    }

    public User findUserByUserName(String userName) {
        return userRepository.findByUserName(userName);
    }
    
    public int getUserId() {
    	
		String username = SecurityContextHolder.getContext().getAuthentication().getName();
		
		int id = userRepository.findByUserName(username).getId();
		
		return id;
    	
    }
    
    public void deleteUser(Integer id) {
        userRepository.deleteById((int)id);
    }
    
    public Gateway findGatewayByID(int id) {
    	
    	return gatewayRepository.findGatewayById(id);
    	
    }
    
    
    public Gateway saveGateway(Gateway gateway) {
    	
    	return gatewayRepository.save(gateway);
    }

    public User saveUser(User user) {
    	
        user.setPassword(bCryptPasswordEncoder.encode(user.getPassword()));
        user.setActive(true);
        //Role userRole = roleRepository.findByRole("ADMIN");
        //user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        return userRepository.save(user);
    }

}
