package kz.predev.database.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kz.predev.database.labs.CurrentLabRepository;

@Service
public class LabService {
	
	@Autowired
    private CurrentLabRepository currentLabRepo;
	
	@Autowired
	private UserService userService;
	
	public int getCurrentLab() {
		
		int userId = userService.getUserId();
		int currentLab = currentLabRepo.findCurrentLabById(userId).getCurrentLabId();
		
		return currentLab;
		
	}

}
