package kz.predev.database.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import kz.predev.database.labs.CurrentLab;
import kz.predev.database.labs.CurrentLabRepository;
import kz.predev.database.services.UserService;

@RequestMapping(path="/lab")
@Controller
public class ActiveLabControllers {
	
	@Autowired
	private UserService userService;
		
	@Autowired
	private CurrentLabRepository currentLabRepo;

	@GetMapping(path="/current")
	  public @ResponseBody CurrentLab getCurrentLab() {
	    // This returns a JSON or XML with the users
		
		int id = userService.getUserId();
	    return currentLabRepo.findCurrentLabById(id);
	    
	  }
	
	 @PutMapping(path="/current")
		public @ResponseBody CurrentLab updateCurrentLab (@RequestBody CurrentLab currentLab) {
			
		 	int currentLabId = currentLab.getCurrentLabId();
			int id = userService.getUserId();
			
			
			currentLab.setCurrentLabId(currentLabId);
			currentLab.setId(id);
			
			currentLabRepo.save(currentLab);
			
			return  currentLab;
	 }
}
