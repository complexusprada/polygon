package kz.predev.database.controllers;

//import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;


import kz.predev.database.User;
import kz.predev.database.UserRepository;
import kz.predev.database.labs.CurrentLab;
import kz.predev.database.labs.CurrentLabRepository;
import kz.predev.database.labs.Gateway;
import kz.predev.database.labs.GatewayRepository;
import kz.predev.database.services.UserService;

@Controller
@RequestMapping(path="/user")
public class UsersController {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private GatewayRepository gatewayRepo;
	
	@Autowired
	private CurrentLabRepository currentLabRepo;
	

	@PostMapping(path="/add")
	public @ResponseBody User addNewUser (@RequestBody User user) {
		
		userService.saveUser(user);
		
		return user;
	}
	
	
	 @GetMapping(path="/all")
	  public @ResponseBody Iterable<User> getAllUsers() {
	    // This returns a JSON or XML with the users
	    return userRepository.findAll();
	    
	  }
	 
	  
	 
	 @DeleteMapping("/{id}")
	 public @ResponseBody String delete(@PathVariable Integer id) {
		 
		 userService.deleteUser(id);
		 
		 return "User was deleted";
		 
	 }

}
