package kz.predev.database.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import kz.predev.database.User;
import kz.predev.database.labs.Gateway;
import kz.predev.database.labs.GatewayRepository;
import kz.predev.database.services.UserService;

@RestController
@RequestMapping(path="/gateway")
public class GatewayControllers {
	
	@Autowired
	private UserService userService;
	
	@Autowired
	private GatewayRepository gatewayRepo;
		
	@PutMapping(path="")
	public @ResponseBody Gateway updateGateWay (@RequestBody Gateway gateway) {
		
		int id = userService.getUserId();
		gateway.setId(id);
		
		return gatewayRepo.save(gateway);
	}
	
	@GetMapping(value="/current")
	public Gateway getGateway() throws Exception {
				
		User currentUser = userService.findUserById(userService.getUserId());
		
		return currentUser.getGateway();
		
	}

}
