package kz.predev.database.labs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="currentLab")
public class CurrentLab {
	
	@Id
	@Column(name="student_id", unique=true)
	private int id;
	
	@Column(name="currentLab")
	private int currentLabId;
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	public int getCurrentLabId() {
		
		return currentLabId;
		
	}
	
	public void setCurrentLabId(int currentLabId) {
		
		this.currentLabId = currentLabId;
		
	}

}
