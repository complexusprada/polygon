package kz.predev.database.labs;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GatewayRepository extends JpaRepository<Gateway, Integer> {
	
	Gateway findGatewayById(int id);

}
