package kz.predev.database.labs;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CurrentLabRepository extends JpaRepository<CurrentLab, Integer> {
	
	CurrentLab findCurrentLabById(int id);

}
