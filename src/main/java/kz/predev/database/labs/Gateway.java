package kz.predev.database.labs;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import kz.predev.database.User;

@Entity
@Table(name="gateway")
public class Gateway {
	
	@Id
	@Column(name="student_id", unique=true)
	private int id;
	
	@Column(name="gateway")
	private String guacip;
	
	
	public int getId() {
		return id;
	}


	public void setId(int id) {
		this.id = id;
	}


	@Column(name="isActive")
	boolean isActive;


	public String getGuacIP() {
		return guacip;
	}


	public void setGuacIP(String guacip) {
		this.guacip = guacip;
	}


	public boolean isActive() {
		return isActive;
	}


	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}
	
	
	
	
}
