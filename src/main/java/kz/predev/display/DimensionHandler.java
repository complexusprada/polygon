package kz.predev.display;

import javax.websocket.Session;

import kz.predev.vcenter.labs.machines.VMmapper;

public class DimensionHandler {
	
	
	private String[] dimensions;
	
	
	public DimensionHandler(Session session) {
		
		this.dimensions = session.getQueryString().split("&");
		
	}
	
	public String getVmName() {
		
		String vmName = dimensions[2].split("=")[1];
		
		return vmName;
		
	}
	
	public String getVmIp() {
		
		String vmName = dimensions[2].split("=")[1];
		
		return VMmapper.map(vmName);
		
		
	}
	
	
	public String getHeight() {
		
		String height = dimensions[1].split("=")[1];
		
		return height;
		
	}
	
	
	public String getWidth() {
		
		String width = dimensions[0].split("=")[1];
		
		return width;

		
	}
	
	
}
