function startLabs() {
	

	$('#day1-start-btn').click(function() {
		 
		 checkCurrentLab();
		 setUpSwitchLabAlert();

	 });
	 
	 $("#procedeSwitchBtn").click(function(){
	
	   	 window.open('/day1/split');
		
	});

}


function setUpSwitchLabAlert(){
	

	var switchModal = new bootstrap.Modal(document.getElementById('switchLabModal'), {
  			keyboard: false
	});

	var labid =  Cookies.get("labid");
	var activeLabId = Cookies.get("active_labid");

	if(activeLabId === "0" || activeLabId === labid){
		
		window.open('/day1/split');
	
	}else if( activeLabId != labid ){
		
		$("#switched-lab-id").text(activeLabId);
		switchModal.show();
		
	}

} 

function checkCurrentLab(){

	$.getJSON('/lab/current', function(data) {
	
		var labid = Cookies.get("labid");
		var activeLabId = JSON.stringify(data.currentLabId);
		
		if(activeLabId === "0"){
			
		  Cookies.set('active_labid', "0");
			
		}else{
			
		  Cookies.set('active_labid', activeLabId);
				
		}
	
	});
	
}




function setUpLabSelectionLogic(id){
		
		//document.cookie = "lab_id="+id;
		Cookies.set('labid', id);
		var labId = "#lab-"+id;
		$("[id^=lab-]").removeClass("active");
		$(labId).addClass("active");
		
}


function setUpBackgroundColor(id){
		
		
		var dayId = "#day-list-item-"+id;
		$("[id^=day-list-item-]").css({"background-color":""});
		$(dayId).css({"background-color":"#7FFFD4"});
		
		
}

function getCurrentModule(){
	
	var pageURL = $(location).attr("href");
	var relativePath = pageURL.split("/")[3];
	
	if(relativePath === ""){
		
		return
	}
	
	
	if(relativePath.includes("module1")){
		
		setUpLabSelectionLogic(11)
		
	}
	
	if(relativePath.includes("module2")){
		
		setUpLabSelectionLogic(21)
		
	}
	
	if(relativePath.includes("module3")){
		
		setUpLabSelectionLogic(31)
		
	}
	
	if(relativePath.includes("module4")){
		
		setUpLabSelectionLogic(41)
		
	}
	
	if(relativePath.includes("module5")){
			
		setUpLabSelectionLogic(51)
		
	}
	
	
}

function setUpEvents(){


		$("[id^=lab-]").click(function(){
		
			var id = "#"+$(this).attr('id');
			var lab_id = id.split("-")[1];
			
			setUpLabSelectionLogic(lab_id);
		
		});
		
		
		$("[id^=nav-link-]").click(function(){
		
			$("[id^=nav-link-]").addClass("collapsed");
			
			$(this).removeClass("collapsed");
			
		
		});
		
		
		$("#bandit-start-btn").click(function(){
		
			window.open("/linux-essentials/bandit");
		
		});
		
}

function navPageContentLoader(){


	$("#nav-link-linux-essentials").click(function(){
	
		$("nav-page-left-switch").load("/navitems/linux-essentials/left.html");
	
	});

}


setUpEvents();
getCurrentModule();
checkCurrentLab();
startLabs();
