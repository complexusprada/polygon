$(document).ready(function() {	
	
	// Get display div from document
	   
	class GuacdConnector{
		
		createInstance(){

			var guac_tunnel = new Guacamole.WebSocketTunnel("webSocket");
            var guac = new Guacamole.Client(guac_tunnel);

			this.guacTunnel = guac_tunnel;
			this.guacClient = guac;
			//guac_tunnel.sendMessage('size', GUAC_WIDTH, GUAC_HEIGHT)
            
            return guac;
		
		}
		
		connectGuacd(guac,VM_NAME){
			

			guac.onerror = function(error) {
                alert(error);
            };
			
			let GUAC_WIDTH = $("#display").width();
            let GUAC_HEIGHT = $(window).height();

			guac.connect("GUAC_WIDTH="+GUAC_WIDTH+"&GUAC_HEIGHT="+GUAC_HEIGHT+"&GUAC_IP="+VM_NAME);
           
           	window.onunload = function() {
	
                guac.disconnect();

            }
            
            var mouse = new Guacamole.Mouse(guac.getDisplay().getElement());

            mouse.onmousedown = 
            mouse.onmouseup   =
            mouse.onmousemove = function(mouseState) {
                guac.sendMouseState(mouseState);
            };
            
            // Keyboard
            var keyboard = new Guacamole.Keyboard(document);
            //keyboard.listenTo(document);

           /* keyboard.onkeydown = function (keysym) {
                guac.sendKeyEvent(1, keysym);
            };
            
            keyboard.onkeyup = function (keysym) {
                guac.sendKeyEvent(0, keysym);
            };*/

			this.guacKeyboard = keyboard;

		}
		
		displayMachine(guac){
			
			let GUAC_WIDTH = $("#display").width();
            let GUAC_HEIGHT = $(window).height();
			
			
			var display = document.getElementById("display");
			
			this.guacTunnel.sendMessage('size', GUAC_WIDTH, GUAC_HEIGHT);

			$("#display").empty();

			display.appendChild(guac.getDisplay().getElement());
			
            //let GUAC_WIDTH = Math.round($(window).width())
            //let GUAC_HEIGHT = Math.round($(window).height())
			
		}

	}
	
	
	function resetGateway(){
	
		var labid = Cookies.get('labid');
		var activeLabId = Cookies.get("active_labid");
		var resettedGateway = { active:false };
		
	
		if(activeLabId === "0" || activeLabId === labid){
					  		
				return;
		}else{
		
			$.ajax({
			
				    url: '/gateway',
				    type: 'PUT',
					data: JSON.stringify(resettedGateway),
					contentType: "application/json; charset=utf-8",
					traditional:true
	
			});
			
		}
		
	}
	

	function doIClone(){
	
			
			var labid = Cookies.get('labid');
			var activeLabId = Cookies.get("active_labid");
			var labEnv = {day:1,labId:labid};
			var currentLabId = { currentLabId:labid };

			if(activeLabId === "0" || activeLabId === labid){
		
		  		$.ajax({
			
				    url: '/iclone/v1/create',
				    type: 'POST',
					data: JSON.stringify(labEnv),
					contentType: "application/json; charset=utf-8",
					traditional:true
	
		  		});
		  				  		
	
			}else{
				
				$.ajax({
			
				    url: '/iclone/v1/switch',
				    type: 'POST',
					data: JSON.stringify(labEnv),
					contentType: "application/json; charset=utf-8",
					traditional:true
	
		  		});
		  					
			}

	}
	
	
	function switchMachine(guacdInstance,guacdClass){
		
		guacdClass.displayMachine(guacdInstance);

	}
	
	
	function setUpSelectionLogic(id){
		
		var machineId = "#machine-"+id;
		$("[id^=machine-]").css({"background-color":""});
		$(machineId).css({"background-color":"#7FFFD4"});
		
	}
	
	function setUpSwitchLabAlert(){

		var switchModal = new bootstrap.Modal(document.getElementById('switchLabModal'), {
	  			keyboard: false
		});
	
		var activeLabId = Cookies.get("active_labid");
		
		$("#switched-lab-id").text(activeLabId);
		switchModal.show();
			
	} 
	
	
	function updateArrayOfMachines(){
		
		var labid = Cookies.get("labid");
		

		
		if (labid === "11") {
			
			clonedMachines = [ KALIguacdClass, WINDOWS01guacdClass, WINDOWS02guacdClass, DCguacdClass ]; //UBUNTU01guacdClass include?

			
		}else if(labid === "12"){
			
			clonedMachines = [ WINDOWS01guacdClass, WINDOWS02guacdClass, DCguacdClass ];

			
		}else if(labid === "13"){
			
			    clonedMachines = [WINDOWS02guacdClass, UBUNTU03guacdClass, DCguacdClass ];

			
		}else if(labid === "14"){
			
			    clonedMachines = [ KALIguacdClass ];

			
			
		}else if(labid === "21"){
			
			    clonedMachines = [ KALIguacdClass, WINDOWS01guacdClass, WINDOWS02guacdClass, DCguacdClass ];

			
			
		}else if(labid === "22"){
			
			    clonedMachines = [  WINDOWS02guacdClass, UBUNTU02guacdClass, DCguacdClass ];

			
		}else if(labid === "23"){
			
			    clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass, DCguacdClass ];

			
			
		}else if(labid === "24"){
			
			    clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass, DCguacdClass ];

			
		}else if(labid === "25"){
			
			    clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass, UBUNTU03guacdClass, DCguacdClass ];

			
		}else if(labid === "26"){
			
			    clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass,  DCguacdClass ];

			
		}else if(labid === "31"){
			
			    clonedMachines = [  WINDOWS02guacdClass, DCguacdClass ];

			
		}else if(labid === "32"){
			
			    clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass, DCguacdClass ];

			
		}else if(labid === "33"){
			
			    clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass, UBUNTU03guacdClass, DCguacdClass ];

			
		}else if(labid === "34"){
			
			    clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass, UBUNTU03guacdClass, DCguacdClass ];

			
		}else if(labid === "41"){
			
			    clonedMachines = [ KALIguacdClass, WINDOWS01guacdClass, WINDOWS02guacdClass, DCguacdClass ];

			
		}else if(labid === "42"){
			
			    clonedMachines = [  WINDOWS02guacdClass, DCguacdClass ];

			
		}else if(labid === "43"){
			
			    clonedMachines = [  WINDOWS02guacdClass, DCguacdClass ];

			
		}else if(labid === "44"){
			
			    clonedMachines = [ KALIguacdClass, WINDOWS01guacdClass, WINDOWS02guacdClass, UBUNTU03guacdClass, DCguacdClass ];

			
		}else if(labid === "45"){
			
			    clonedMachines = [ KALIguacdClass, WINDOWS01guacdClass, UBUNTU03guacdClass, DCguacdClass ];

			
		}else if(labid === "46"){
			
			    clonedMachines = [ KALIguacdClass, WINDOWS01guacdClass, WINDOWS02guacdClass, UBUNTU03guacdClass, DCguacdClass ];

		
		}else if(labid === "51"){
			
			    clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass, UBUNTU03guacdClass, DCguacdClass ];

			
		}else if(labid === "52"){
			
			    clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass, UBUNTU01guacdClass, UBUNTU03guacdClass,  DCguacdClass ];

			
		}else if(labid === "53"){
			
			    clonedMachines = [ WINDOWS02guacdClass, UBUNTU02guacdClass,  DCguacdClass ];

			
		}else if(labid === "54"){
			
			    clonedMachines = [ WINDOWS01guacdClass, WINDOWS02guacdClass, UBUNTU03guacdClass,  DCguacdClass ];

			
		}else if(labid === "55"){
			
			    clonedMachines = [ WINDOWS02guacdClass, UBUNTU02guacdClass,  DCguacdClass ];

		}
		
	}
	
	
	function hideElements(){
		
		//$("#offcanvas-theory-tab").hide();
				
	}
	
	
	function setUpMachines(){
		
		var labid = Cookies.get("labid");
		

			$("#machine-7").hide();
			$("#machine-6").hide();
			$("#machine-5").hide();
			$("#machine-4").hide();
			$("#machine-3").hide();
			$("#machine-2").hide();
			$("#machine-1").hide();
		
		
		function lab11(){
			
			$("#machine-7").show();
			//$("#machine-4").show(); include?
			$("#machine-1").show();
			$("#machine-2").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(1);

			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();

			WINDOWS01guacdClass = new GuacdConnector();
			WINDOWS01guacdInstance = WINDOWS01guacdClass.createInstance();

			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();

			/*UBUNTU01guacdClass = new GuacdConnector();
			UBUNTU01guacdInstance = UBUNTU01guacdClass.createInstance();*/

			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");

			WINDOWS01guacdClass.connectGuacd(WINDOWS01guacdInstance,"WINDOWS01");
			
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");

			//UBUNTU01guacdClass.connectGuacd(UBUNTU01guacdInstance,"UBUNTU01");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			setUpKeyboardLogic(KALIguacdClass);

			clonedMachines = [ KALIguacdClass, WINDOWS01guacdClass, WINDOWS02guacdClass, DCguacdClass ]; //UBUNTU01guacdClass

			
		}
		
		
		function lab12(){
			
			$("#machine-7").show();
			$("#machine-2").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(2);

			WINDOWS01guacdClass = new GuacdConnector();
			WINDOWS01guacdInstance = WINDOWS01guacdClass.createInstance();
	
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			WINDOWS01guacdClass.displayMachine(WINDOWS01guacdInstance);
			
			WINDOWS01guacdClass.connectGuacd(WINDOWS01guacdInstance,"WINDOWS01");
			
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			
			setUpKeyboardLogic(WINDOWS01guacdClass);

			clonedMachines = [ WINDOWS01guacdClass, WINDOWS02guacdClass, DCguacdClass ];
			
		}
		
		
		function lab13(){
			
			$("#machine-7").show();
			$("#machine-6").show();
			$("#machine-3").show();
			
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			UBUNTU03guacdClass = new GuacdConnector();
			UBUNTU03guacdInstance = UBUNTU03guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
						
			WINDOWS02guacdClass.displayMachine(WINDOWS02guacdInstance);
			
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");

			UBUNTU03guacdClass.connectGuacd(UBUNTU03guacdInstance,"UBUNTU03");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
						
			setUpKeyboardLogic(WINDOWS02guacdClass);
			
			clonedMachines = [WINDOWS02guacdClass, UBUNTU03guacdClass, DCguacdClass ];

			
		}
		
		function lab14(){
			
			$("#machine-1").show();
			
			setUpSelectionLogic(1);
			
			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();
			
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");
						
			setUpKeyboardLogic(KALIguacdClass);
			
			clonedMachines = [ KALIguacdClass ];

			
		}
		
		
		function lab21(){
			
			$("#machine-7").show();
			$("#machine-1").show();
			$("#machine-2").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(1);

			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();

			WINDOWS01guacdClass = new GuacdConnector();
			WINDOWS01guacdInstance = WINDOWS01guacdClass.createInstance();
	
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");

			WINDOWS01guacdClass.connectGuacd(WINDOWS01guacdInstance,"WINDOWS01");
			
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			setUpKeyboardLogic(KALIguacdClass);
			
			clonedMachines = [ KALIguacdClass, WINDOWS01guacdClass, WINDOWS02guacdClass, DCguacdClass ];

			
		}
		
		
		function lab22(){
			
			$("#machine-7").show();
			$("#machine-5").show();
			$("#machine-3").show();
	
			setUpSelectionLogic(3);

			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();

			UBUNTU02guacdClass = new GuacdConnector();
			UBUNTU02guacdInstance = UBUNTU02guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
						
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");

			UBUNTU02guacdClass.connectGuacd(UBUNTU02guacdInstance,"UBUNTU02");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			WINDOWS02guacdClass.displayMachine(WINDOWS02guacdInstance);
			
			setUpKeyboardLogic(WINDOWS02guacdClass);

			clonedMachines = [  WINDOWS02guacdClass, UBUNTU02guacdClass, DCguacdClass ];

			
		}
		
		
		function lab23(){
			
			$("#machine-7").show();
			$("#machine-1").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(3);

			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();
	
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
		
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");
			
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			setUpKeyboardLogic(KALIguacdClass);
			
			clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass, DCguacdClass ];

		}
		
		
		function lab24(){
			
			$("#machine-7").show();
			$("#machine-1").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(1);

			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();
	
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");
			
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			setUpKeyboardLogic(KALIguacdClass);
			
			clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass, DCguacdClass ];

		}
		
		
		function lab25(){
			
			$("#machine-7").show();
			$("#machine-6").show();
			$("#machine-1").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(1);

			
			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();
	
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			UBUNTU03guacdClass = new GuacdConnector();
			UBUNTU03guacdInstance = UBUNTU03guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");
			
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");

			UBUNTU03guacdClass.connectGuacd(UBUNTU03guacdInstance,"UBUNTU03");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			setUpKeyboardLogic(KALIguacdClass);
			
			clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass, UBUNTU03guacdClass, DCguacdClass ];
			
			
		}
		
		
		function lab26(){
			
			$("#machine-7").show();
			$("#machine-1").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(1);

			
			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();

			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");
			
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			setUpKeyboardLogic(KALIguacdClass);

			
			clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass,  DCguacdClass ];
			
			
		}
		
		
		function lab31(){
			
			$("#machine-7").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(3);

			
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
						
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			WINDOWS02guacdClass.displayMachine(WINDOWS02guacdInstance);
			
			setUpKeyboardLogic(WINDOWS02guacdClass);

			clonedMachines = [  WINDOWS02guacdClass, DCguacdClass ];

			
		}
		
		
		function lab32(){
			
			$("#machine-7").show();
			$("#machine-1").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(1);

			
			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();
	
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");
			
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			setUpKeyboardLogic(KALIguacdClass);
			
			clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass, DCguacdClass ];

			
		}
		
		
		function lab33(){
			
			$("#machine-7").show();
			$("#machine-6").show();
			$("#machine-1").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(1);

			
			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();
	
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			UBUNTU03guacdClass = new GuacdConnector();
			UBUNTU03guacdInstance = UBUNTU03guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");
			
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			
			UBUNTU03guacdClass.connectGuacd(UBUNTU03guacdInstance,"UBUNTU03");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			setUpKeyboardLogic(KALIguacdClass);
			
			clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass, UBUNTU03guacdClass, DCguacdClass ];
			
		}
		
		
		function lab34(){
			
			$("#machine-7").show();
			$("#machine-6").show();
			$("#machine-1").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(1);

			
			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();

			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			UBUNTU03guacdClass = new GuacdConnector();
			UBUNTU03guacdInstance = UBUNTU03guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");
			
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			
			UBUNTU03guacdClass.connectGuacd(UBUNTU03guacdInstance,"UBUNTU03");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			setUpKeyboardLogic(KALIguacdClass);
			
			clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass, UBUNTU03guacdClass, DCguacdClass ];

			
		}
		
		
		function lab41(){
			
			$("#machine-7").show();
			$("#machine-1").show();
			$("#machine-2").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(1);

			
			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();

			WINDOWS01guacdClass = new GuacdConnector();
			WINDOWS01guacdInstance = WINDOWS01guacdClass.createInstance();
	
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");
			
			WINDOWS01guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS01");

			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
						
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			setUpKeyboardLogic(KALIguacdClass);
			
			clonedMachines = [ KALIguacdClass, WINDOWS01guacdClass, WINDOWS02guacdClass, DCguacdClass ];

		}
		
		function lab42(){
			
			$("#machine-7").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(3);

				
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
						
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			WINDOWS02guacdClass.displayMachine(WINDOWS02guacdInstance);
			
			setUpKeyboardLogic(WINDOWS02guacdClass);

			clonedMachines = [  WINDOWS02guacdClass, DCguacdClass ];

			
		}
		
		function lab43(){
			
			$("#machine-7").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(3);

			
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
						
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			WINDOWS02guacdClass.displayMachine(WINDOWS02guacdInstance);
			
			setUpKeyboardLogic(WINDOWS02guacdClass);
			
			clonedMachines = [  WINDOWS02guacdClass, DCguacdClass ];

			
		}
		
		function lab44(){
			
			$("#machine-7").show();
			$("#machine-6").show();
			$("#machine-1").show();
			$("#machine-2").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(1);

			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();

			WINDOWS01guacdClass = new GuacdConnector();
			WINDOWS01guacdInstance = WINDOWS01guacdClass.createInstance();
			
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			UBUNTU03guacdClass = new GuacdConnector();
			UBUNTU03guacdInstance = UBUNTU03guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");
			
			WINDOWS01guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS01");

			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			
			UBUNTU03guacdClass.connectGuacd(UBUNTU03guacdInstance,"UBUNTU03");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			setUpKeyboardLogic(KALIguacdClass);
			
			clonedMachines = [ KALIguacdClass, WINDOWS01guacdClass, WINDOWS02guacdClass, UBUNTU03guacdClass, DCguacdClass ];

			
		}
		
		function lab45(){
			
			$("#machine-7").show();
			$("#machine-6").show();
			$("#machine-1").show();
			$("#machine-2").show();
			
			setUpSelectionLogic(1);

			
			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();

			WINDOWS01guacdClass = new GuacdConnector();
			WINDOWS01guacdInstance = WINDOWS01guacdClass.createInstance();
	
			UBUNTU03guacdClass = new GuacdConnector();
			UBUNTU03guacdInstance = UBUNTU03guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");
			
			WINDOWS01guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS01");
			
			UBUNTU03guacdClass.connectGuacd(UBUNTU03guacdInstance,"UBUNTU03");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			setUpKeyboardLogic(KALIguacdClass);
			
			clonedMachines = [ KALIguacdClass, WINDOWS01guacdClass, UBUNTU03guacdClass, DCguacdClass ];

			
		}
		
		function lab46(){
			
			$("#machine-7").show();
			$("#machine-6").show();
			$("#machine-1").show();
			$("#machine-2").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(1);

			
			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();

			WINDOWS01guacdClass = new GuacdConnector();
			WINDOWS01guacdInstance = WINDOWS01guacdClass.createInstance();
	
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			UBUNTU03guacdClass = new GuacdConnector();
			UBUNTU03guacdInstance = UBUNTU03guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");
			
			WINDOWS01guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS01");

			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			
			UBUNTU03guacdClass.connectGuacd(UBUNTU03guacdInstance,"UBUNTU03");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			setUpKeyboardLogic(KALIguacdClass);
			
			clonedMachines = [ KALIguacdClass, WINDOWS01guacdClass, WINDOWS02guacdClass, UBUNTU03guacdClass, DCguacdClass ];

			
		}
		
		function lab51(){
			
			$("#machine-7").show();
			$("#machine-6").show();
			$("#machine-1").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(1);

			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();
	
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			UBUNTU03guacdClass = new GuacdConnector();
			UBUNTU03guacdInstance = UBUNTU03guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");
			
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			
			UBUNTU03guacdClass.connectGuacd(UBUNTU03guacdInstance,"UBUNTU03");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			setUpKeyboardLogic(KALIguacdClass);
			
			clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass, UBUNTU03guacdClass, DCguacdClass ];

			
		}
		
		function lab52(){
			
			$("#machine-7").show();
			$("#machine-4").show();
			$("#machine-6").show();
			$("#machine-1").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(1);

			
			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();
	
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			UBUNTU01guacdClass = new GuacdConnector();
			UBUNTU01guacdInstance = UBUNTU01guacdClass.createInstance();
	
			UBUNTU03guacdClass = new GuacdConnector();
			UBUNTU03guacdInstance = UBUNTU03guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");
			
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			
			UBUNTU01guacdClass.connectGuacd(UBUNTU01guacdInstance,"UBUNTU01");
			
			UBUNTU03guacdClass.connectGuacd(UBUNTU03guacdInstance,"UBUNTU03");

			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			setUpKeyboardLogic(KALIguacdClass);
			
			clonedMachines = [ KALIguacdClass, WINDOWS02guacdClass, UBUNTU01guacdClass, UBUNTU03guacdClass,  DCguacdClass ];

			
		}
		
		function lab53(){
			
			$("#machine-7").show();
			$("#machine-5").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(3);

			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();

			UBUNTU02guacdClass = new GuacdConnector();
			UBUNTU02guacdInstance = UBUNTU02guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
						
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			
			UBUNTU02guacdClass.connectGuacd(UBUNTU02guacdInstance,"UBUNTU02");
			
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			WINDOWS02guacdClass.displayMachine(WINDOWS02guacdInstance);
			
			setUpKeyboardLogic(WINDOWS02guacdClass);			
			
			clonedMachines = [ WINDOWS02guacdClass, UBUNTU02guacdClass,  DCguacdClass ];

			
		}
		
		function lab54(){
			
			$("#machine-7").show();
			$("#machine-6").show();
			$("#machine-2").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(2);
			
			WINDOWS01guacdClass = new GuacdConnector();
			WINDOWS01guacdInstance = WINDOWS01guacdClass.createInstance();
	
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
	
			UBUNTU03guacdClass = new GuacdConnector();
			UBUNTU03guacdInstance = UBUNTU03guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			
			
			WINDOWS01guacdClass.connectGuacd(WINDOWS01guacdInstance,"WINDOWS01");
			
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			
			UBUNTU03guacdClass.connectGuacd(UBUNTU03guacdInstance,"UBUNTU03");

			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			WINDOWS01guacdClass.displayMachine(WINDOWS01guacdInstance);
			
			setUpKeyboardLogic(WINDOWS01guacdClass);			
			
			clonedMachines = [ WINDOWS01guacdClass, WINDOWS02guacdClass, UBUNTU03guacdClass,  DCguacdClass ];

			
		}
		
		function lab55(){
			
			$("#machine-7").show();
			$("#machine-5").show();
			$("#machine-3").show();
			
			setUpSelectionLogic(3);

	
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();

			UBUNTU02guacdClass = new GuacdConnector();
			UBUNTU02guacdInstance = UBUNTU02guacdClass.createInstance();
	
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
						
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			
			UBUNTU02guacdClass.connectGuacd(UBUNTU02guacdInstance,"UBUNTU02");

			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			
			WINDOWS02guacdClass.displayMachine(WINDOWS02guacdInstance);
			
			setUpKeyboardLogic(WINDOWS02guacdClass);			
			
			clonedMachines = [ WINDOWS02guacdClass, UBUNTU02guacdClass,  DCguacdClass ];

			
		}
		
		
		switch(labid){
			
			case "11":
				lab11();
				break;
			case "12":
				lab12();
				break;
			case "13":
				lab13();
				break;
			case "14":
				lab14();
				break;
			case "21":
				lab21();
				break;
			case "22":
				lab22();
				break;
			case "23":
				lab23();
				break;
			case "24":
				lab24();
				break;
			case "25":
				lab25();
				break;
			case "26":
				lab26();
				break;
			case "31":
				lab31();
				break;
			case "32":
				lab32();
				break;
			case "33":
				lab33();
				break;
			case "34":
				lab34();
				break;
			case "41":
				lab44();
				break;
			case "42":
				lab42();
				break;
			case "43":
				lab43();
				break;
			case "44":
				lab44();
				break;
			case "45":
				lab45();
				break;
			case "46":
				lab46();
				break;
			case "51":
				lab51();
				break;
			case "52":
				lab52();
				break;
			case "53":
				lab53();
				break;
			case "54":
				lab54();
				break;
			case "55":
				lab55();
				break;		
			
		}
		
	}
	
	function unsetKeyboard(guacdKeyboard){
			
		    guacdKeyboard.onkeydown = function (keysym) {};
            guacdKeyboard.onkeyup = function (keysym) {};
		
	}
	
	function setKeyboard(guacKeyboard,guacInstance){
		
		     guacKeyboard.onkeydown = function (keysym) {
                guacInstance.sendKeyEvent(1, keysym);
            };
            
            guacKeyboard.onkeyup = function (keysym) {
                guacInstance.sendKeyEvent(0, keysym);
            };
		
	}
	
	function setUpKeyboardLogic(guacClass){
		
		$.each( clonedMachines, function( index, value ) {
					
				unsetKeyboard(value.guacKeyboard);			

		});
		
		setKeyboard(guacClass.guacKeyboard, guacClass.guacClient);
		
		
	}
	
	function updateLabID(id){
			
		var activeLabId = Cookies.get("active_labid");
		
	
		if(activeLabId === "0" || activeLabId === id){
						  		
				return;
		}else{
		
			setUpSwitchLabAlert();
		
			$("#procedeSwitchBtn").click(function(){
				
				 Cookies.set("labid",id);
				 
				 let daynumber = id.charAt(0);
				 
	   			 window.open('/day'+daynumber+'/split',"_self");
		
			});
			
		}
	
	}
	
	function setUpSwitchLab(){
	
		$("[id^=labid-]").click(function(){
		
			var id = "#"+$(this).attr('id');
			var lab_id = id.split("-")[1];
			
			
			updateLabID(lab_id);
		
		});
	
	}
	
	function resizeScreen(){
				
		
		$("#full-screen-item").click(function() {
			
			if( inFullScreen == false ) {
				
				$("#split-content").toggleClass('col-6  col-3');
				$("#display").toggleClass('col-6  col-9');
				
				let GUAC_WIDTH = $("#display").width();
            	let GUAC_HEIGHT = $(window).height();

				$("#home-tab").empty().append('<i class="bi bi-card-text"></i>');
				$("#profile-tab").empty().append('<i class="bi bi-laptop"></i>');
				$("#navbarDarkDropdownMenuLink").hide();
				$("#lab-scheme-tab").hide();
				//$("#home-tab").hide();
				
				//$("#offcanvas-theory-tab").show();
				
				$('#machine-container').find('.col-md-4').toggleClass('col-md-4 tempor');
				$('#machine-container').find('.col-md-8').toggleClass('col-md-8 tempor');
				
				//$("#borderedTab").find('.nav-item').attr("aria-selected","false");
				$("#profile-tab").click();
				
				try{
					
					$.each( clonedMachines, function( index, value ) {
						
							value.guacTunnel.sendMessage('size', GUAC_WIDTH, GUAC_HEIGHT);
							
					});
					
					var labid = Cookies.get("labid");
		

		
					if (labid === "22" || labid === "53" || labid === "55") {
			
						//clonedMachines = [ KALIguacdClass, WINDOWS01guacdClass, WINDOWS02guacdClass, DCguacdClass ]; //UBUNTU01guacdClass include?
						
						UBUNTU02guacdClass.guacTunnel.disconnect();
						UBUNTU02guacdClass = new GuacdConnector();
						UBUNTU02guacdInstance = UBUNTU02guacdClass.createInstance();
						UBUNTU02guacdClass.connectGuacd(UBUNTU02guacdInstance,"UBUNTU02");
						updateArrayOfMachines();
					
			
					}
					
					
					
				}catch(err){
					
					//Do nothing
					
				}

				inFullScreen = true;
			
			}else{
				
				$("#split-content").toggleClass('col-3  col-6');
				$("#display").toggleClass('col-9  col-6');
				
				$("#navbarDarkDropdownMenuLink").show();
				$("#home-tab").show();
				$("#lab-scheme-tab").show();
				
				//$("#offcanvas-theory-tab").hide();
				

				$("#home-tab").empty().text('Лаборатория');
				$("#profile-tab").empty().text('Машины');
				
				$('#machine-container').find('.tempor').toggleClass('tempor col-md-4');
				$('#machine-container').find('.tempor').toggleClass('tempor col-md-8');
				
				let GUAC_WIDTH = $("#display").width();
            	let GUAC_HEIGHT = $(window).height();
				
				try {
					
					$.each( clonedMachines, function( index, value ) {
						
							value.guacTunnel.sendMessage('size', GUAC_WIDTH, GUAC_HEIGHT);
	
					});
					
					var labid = Cookies.get("labid");
		

		
					if (labid === "22" || labid === "53" || labid === "55") {
			
						//clonedMachines = [ KALIguacdClass, WINDOWS01guacdClass, WINDOWS02guacdClass, DCguacdClass ]; //UBUNTU01guacdClass include?
						
						UBUNTU02guacdClass.guacTunnel.disconnect();
						UBUNTU02guacdClass = new GuacdConnector();
						UBUNTU02guacdInstance = UBUNTU02guacdClass.createInstance();
						UBUNTU02guacdClass.connectGuacd(UBUNTU02guacdInstance,"UBUNTU02");
						UBUNTU02guacdClass.displayMachine(UBUNTU02guacdInstance);
						updateArrayOfMachines();
					
			
					}
					
			
					
				}catch(err){
					
					//You do nothing
					
				}
				
				inFullScreen = false;
				
			}
												
		});
		
		

		
	}
	

	
	function activeMachineDisplayer(){
		
		
		$("#switch-machine-1").click(function() {
				
				setUpSelectionLogic(1);
				setUpKeyboardLogic(KALIguacdClass);
				//$("#machine-container > .col-md-8").removeClass("col-md-8");
												
		});
		$("#switch-machine-2").click(function() {
			
				setUpSelectionLogic(2);
				setUpKeyboardLogic(WINDOWS01guacdClass);


	 	});
		
		$("#switch-machine-3").click(function() {
			
  				setUpSelectionLogic(3);
				setUpKeyboardLogic(WINDOWS02guacdClass);


	 	});
	
		$("#switch-machine-4").click(function() {
			
  				setUpSelectionLogic(4);
				setUpKeyboardLogic(UBUNTU01guacdClass);

	
	 	});
		
		$("#switch-machine-5").click(function() {
			
  				setUpSelectionLogic(5);
				setUpKeyboardLogic(UBUNTU02guacdClass);

	
	 	});
	
		$("#switch-machine-6").click(function() {
			
  				setUpSelectionLogic(6);
				setUpKeyboardLogic(UBUNTU03guacdClass);

	
	 	});
	
		$("#switch-machine-7").click(function() {
			
  				setUpSelectionLogic(7);
				setUpKeyboardLogic(DCguacdClass);

	
	 	});

	}
	
	
	function setUpEvents(){
		
		$("#switch-machine-1").click(function() {
  			switchMachine(KALIguacdInstance, KALIguacdClass);
	 	});
	
		$("#switch-machine-2").click(function() {
  			switchMachine(WINDOWS01guacdInstance,WINDOWS01guacdClass);
	 	});
	
		$("#switch-machine-3").click(function() {
  			switchMachine(WINDOWS02guacdInstance,WINDOWS02guacdClass);
	 	});
	
		$("#switch-machine-4").click(function() {
  			switchMachine(UBUNTU01guacdInstance,UBUNTU01guacdClass);
	 	});
		
		$("#switch-machine-5").click(function() {
  			switchMachine(UBUNTU02guacdInstance,UBUNTU02guacdClass);
	 	});
	
		$("#switch-machine-6").click(function() {
  			switchMachine(UBUNTU03guacdInstance,UBUNTU03guacdClass);
	 	});
	
		$("#switch-machine-7").click(function() {
  			switchMachine( DCguacdInstance, DCguacdClass);
	 	});
		
	}
	
		

	function setupReconnecter(){
		
		$("#kaliReconnect").click(function() {
			
  			//setupLoadingCube();
			setUpSelectionLogic(1);
			KALIguacdClass.guacTunnel.disconnect();
			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();	
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");
			KALIguacdClass.displayMachine(KALIguacdInstance);
			
			updateArrayOfMachines();

	 	});
	
		$("#windows01Reconnect").click(function() {
  			
			//setupLoadingCube();
			setUpSelectionLogic(2);
			WINDOWS01guacdClass.guacTunnel.disconnect();
			WINDOWS01guacdClass = new GuacdConnector();
			WINDOWS01guacdInstance = WINDOWS01guacdClass.createInstance();
			WINDOWS01guacdClass.connectGuacd(WINDOWS01guacdInstance,"WINDOWS01");
			WINDOWS01guacdClass.displayMachine(WINDOWS01guacdInstance);
			
			updateArrayOfMachines();
			
	 	});
	
		$("#windows02Reconnect").click(function() {
  			
			//setupLoadingCube();
			setUpSelectionLogic(3);
			WINDOWS02guacdClass.guacTunnel.disconnect();
			WINDOWS02guacdClass = new GuacdConnector();
			WINDOWS02guacdInstance = WINDOWS02guacdClass.createInstance();
			WINDOWS02guacdClass.connectGuacd(WINDOWS02guacdInstance,"WINDOWS02");
			WINDOWS02guacdClass.displayMachine(WINDOWS02guacdInstance);
			
			updateArrayOfMachines();
			
	 	});
	
		$("#ubuntu01Reconnect").click(function() {
			
			//setupLoadingCube();
			setUpSelectionLogic(4);
			UBUNTU01guacdClass.guacTunnel.disconnect();
			UBUNTU01guacdClass = new GuacdConnector();
			UBUNTU01guacdInstance = UBUNTU01guacdClass.createInstance();
			UBUNTU01guacdClass.connectGuacd(UBUNTU01guacdInstance,"UBUNTU01");
			UBUNTU01guacdClass.displayMachine(UBUNTU01guacdInstance);
			
			updateArrayOfMachines();

	 	});
	
		$("#ubuntu02Reconnect").click(function() {
			
			//setupLoadingCube();
			setUpSelectionLogic(5);
			UBUNTU02guacdClass.guacTunnel.disconnect();
			UBUNTU02guacdClass = new GuacdConnector();
			UBUNTU02guacdInstance = UBUNTU02guacdClass.createInstance();
			UBUNTU02guacdClass.connectGuacd(UBUNTU02guacdInstance,"UBUNTU02");
			UBUNTU02guacdClass.displayMachine(UBUNTU02guacdInstance);
			
			updateArrayOfMachines();

	 	});
	
		$("#ubuntu03Reconnect").click(function() {
			
			//setupLoadingCube();
			setUpSelectionLogic(6);
			UBUNTU03guacdClass.guacTunnel.disconnect();
			UBUNTU03guacdClass = new GuacdConnector();
			UBUNTU03guacdInstance = UBUNTU03guacdClass.createInstance();
			UBUNTU03guacdClass.connectGuacd(UBUNTU03guacdInstance,"UBUNTU03");
			UBUNTU03guacdClass.displayMachine(UBUNTU03guacdInstance);
			
			updateArrayOfMachines();

	 	});
		
		$("#DCReconnect").click(function() {
			
			//setupLoadingCube();
			setUpSelectionLogic(7);
			DCguacdClass.guacTunnel.disconnect();
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");
			DCguacdClass.displayMachine(DCguacdInstance);
			
			updateArrayOfMachines();

	 	});
	
		
		
	}
	
	function checkConnection(){
		
		if(!KALIguacdClass.guacTunnel.isConnected()){
			
			KALIguacdClass = new GuacdConnector();
			KALIguacdInstance = KALIguacdClass.createInstance();	
			KALIguacdClass.connectGuacd(KALIguacdInstance,"KALI");
			KALIguacdClass.displayMachine(KALIguacdInstance);


		}else if(!DCguacdClass.guacTunnel.isConnected()){
			
			DCguacdClass = new GuacdConnector();
			DCguacdInstance = DCguacdClass.createInstance();
			DCguacdClass.connectGuacd(DCguacdInstance,"DC");


		}else if(!UBUNTU02guacdClass.guacTunnel.isConnected()){
			
			UBUNTU02guacdClass = new GuacdConnector();
			UBUNTU02guacdInstance = UBUNTU02guacdClass.createInstance();
			UBUNTU02guacdClass.connectGuacd(UBUNTU02guacdInstance,"UBUNTU02");
			
		}else if(!WINDOWS01guacdClass.guacTunnel.isConnected()){
			
			WINDOWS01guacdClass = new GuacdConnector();
			WINDOWS01guacdInstance = WINDOWS01guacdClass.createInstance();
			WINDOWS01guacdClass.connectGuacd(WINDOWS01guacdInstance,"WINDOWS01");

		}
		
		
	}
	
	function updateCurrentLabCookie(){
	
		$.getJSON('/lab/current', function(data) {
		
			
			Cookies.set('active_labid', JSON.stringify(data.currentLabId));
		
		});
		
	}
	
	function setupLoadingCube(){
		
		var cube = '<div id="guacamole-spinner"><div class="container"> <div class="h1Container"> <div class="cube h1 w1 l1"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h1 w1 l2"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h1 w1 l3"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h1 w2 l1"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h1 w2 l2"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h1 w2 l3"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h1 w3 l1"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h1 w3 l2"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h1 w3 l3"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div></div><div class="h2Container"> <div class="cube h2 w1 l1"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h2 w1 l2"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h2 w1 l3"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h2 w2 l1"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h2 w2 l2"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h2 w2 l3"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h2 w3 l1"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h2 w3 l2"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h2 w3 l3"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div></div><div class="h3Container"> <div class="cube h3 w1 l1"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h3 w1 l2"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h3 w1 l3"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h3 w2 l1"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h3 w2 l2"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h3 w2 l3"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h3 w3 l1"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h3 w3 l2"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div><div class="cube h3 w3 l3"> <div class="face top"></div><div class="face left"></div><div class="face right"></div></div></div></div></div>';

		$("#display").empty();
		$("#display").append(cube);
		
	}
	
	function pagenation(){
		
		var labNum = Cookies.get("labid");
		var exerciseNum = labNum.charAt(0) +"." + labNum.charAt(1);
		var moduleNum = $("#theory-content").attr("module-number");
		var main_page = "/modules/"+ moduleNum + "/" + labNum + "/page_1.html";
		var navigator_items = "/modules/"+ moduleNum + "/" + labNum + "/navigator.html";
		$("#exercise-number").text(exerciseNum);
		$("#page-container").load(main_page);
		$("#page-navigation-items").load(navigator_items, function(){
		
			$("[id^=module-]").click( function(){
			
				$("#pagination-items>li.active").removeClass("active");
				$(this).addClass("active");
				
				var id = $(this).attr('id');
				var page_number = id.split("-")[3];
				var page_url = "/modules/"+ moduleNum + "/" + labNum + "/page_" + page_number + ".html";
				$("#page-container").load(page_url);
				$("#borderedTabContent").animate({scrollTop: '0px'}, 300);
			
			});
		
		});
		
		
	}
	
	function setUpEnviroment(){
			
			var once = false;
			
			resetGateway();
			
			$("#machine-6").hide();
			$("#machine-5").hide();
			$("#machine-3").hide();
      		
      		var checker = setInterval(function(){
      			
      			 $.getJSON('/gateway/current', function(data) {
 	        		
	 					if(data.active == false){
							
							console.log(data.active);
							
							if(once == false) {
								
								doIClone();
								
								once = true;
								
							}
	 							 					
	 				}else {
														
							setUpMachines();
							activeMachineDisplayer();
							setUpEvents();
							setupReconnecter();
							resizeScreen();
							setUpSwitchLab();
							updateCurrentLabCookie();
							

	 						clearInterval(checker);

	 				}
 	        });
      			
      		}, 2000);
      		
      	}
	
	
	hideElements();
	
	
	$("#split-content").height($(window).height());
	var newheight = $(window).outerHeight(true) - $("#borderedTab").outerHeight(true);
	$("#borderedTabContent").height(newheight);
	
	var inFullScreen = false;
		
	var KALIguacdClass;
	var KALIguacdInstance;

	var WINDOWS01guacdClass;
	var WINDOWS01guacdInstance;
	
	var WINDOWS02guacdClass;
	var WINDOWS02guacdInstance;
	
	var UBUNTU01guacdClass;
	var UBUNTU01guacdInstance;

	var UBUNTU02guacdClass;
	var UBUNTU02guacdInstance;
	
	var UBUNTU03guacdClass;
	var UBUNTU03guacdInstance;
	
	var DCguacdClass;
	var DCguacdInstance;
	
	var clonedMachines;
	
	pagenation();
	setUpEnviroment();

});



