class GuacdConnector{
		
		createInstance(){

			var guac_tunnel = new Guacamole.WebSocketTunnel("/day1/webSocket");
            var guac = new Guacamole.Client(guac_tunnel);

			
			this.guacTunnel = guac_tunnel;
			this.guacClient = guac;
			            
            return guac;
		
		}
		
		connectGuacd(guac,VM_NAME){
			

			guac.onerror = function(error) {
                alert(error);
            };
			
			let GUAC_WIDTH = $("#display").width();
            let GUAC_HEIGHT = $(window).height();

			var display = document.getElementById("display");
			
			$("#display").empty();

			display.appendChild(guac.getDisplay().getElement());

			guac.connect("GUAC_WIDTH="+GUAC_WIDTH+"&GUAC_HEIGHT="+GUAC_HEIGHT+"&GUAC_IP="+VM_NAME);
           
			window.onunload = function() {
	
                guac.disconnect();

            }
            
            var mouse = new Guacamole.Mouse(guac.getDisplay().getElement());

            mouse.onmousedown = 
            mouse.onmouseup   =
            mouse.onmousemove = function(mouseState) {
                guac.sendMouseState(mouseState);
            };
            
            // Keyboard
            var keyboard = new Guacamole.Keyboard(document);
            //keyboard.listenTo(document);

           /* keyboard.onkeydown = function (keysym) {
                guac.sendKeyEvent(1, keysym);
            };
            
            keyboard.onkeyup = function (keysym) {
                guac.sendKeyEvent(0, keysym);
            };*/

			this.guacKeyboard = keyboard;

		}
		
		displayMachine(guac){
			
	
		
			let GUAC_WIDTH = $("#display").width();
            let GUAC_HEIGHT = $(window).height();
			
						
			this.guacTunnel.sendMessage('size', GUAC_WIDTH, GUAC_HEIGHT)
			

			var display = document.getElementById("display");
			
			$("#display").empty();

			display.appendChild(guac.getDisplay().getElement());
			
            //let GUAC_WIDTH = Math.round($(window).width())
            //let GUAC_HEIGHT = Math.round($(window).height())
			
		}

	}
	
	function setKeyboard(guacKeyboard,guacInstance){
			
	     guacKeyboard.onkeydown = function (keysym) {
	        guacInstance.sendKeyEvent(1, keysym);
	    };
	    
	    guacKeyboard.onkeyup = function (keysym) {
	        guacInstance.sendKeyEvent(0, keysym);
	    };
			
	}

	function setupBanditMachine(){
		
		BANDITguacdClass = new GuacdConnector();
		
	
		BANDITguacdInstance = BANDITguacdClass.createInstance();
		BANDITguacdClass.displayMachine(BANDITguacdInstance);
			
		BANDITguacdClass.connectGuacd(BANDITguacdInstance,"BANDIT");
		
		BANDITguacdInstance.sendSize(GUAC_WIDTH,GUAC_HEIGHT);
	
		setKeyboard(BANDITguacdClass.guacKeyboard, BANDITguacdClass.guacClient);
		
	
	}

	function screenToggle(){
		
		$("#full-screen-item").click(function() {
			
			BANDITguacdClass.guac_tunnel.sendMessage('size', GUAC_WIDTH, GUAC_HEIGHT);
			
		});
		
	}
	
	function pagination(){
		
		var inital_page = "/bandit/page_1.html";
		var navigator_items = "/bandit/navigator.html"
		
		$("#walkcont-1").load("/bandit/walkthrough/lvl_1.html");
		$("#page-container").load(inital_page);
		
		$("#page-navigation-items").load(navigator_items, function(){
		
			$("[id^=banditcontent-]").click( function(){
			
				$("#pagination-items>li.active").removeClass("active");
				$(this).addClass("active");
				
				var id = $(this).attr('id');
				var page_number = id.split("-")[2];
				var page_url = "/bandit" + "/page_" + page_number + ".html";
				$("#page-container").load(page_url);
				$("#bandit-tab").animate({scrollTop: '0px'}, 300);
			
			});
		
		});
		
	}
	
	function walkthroughEventSetter(){
		
		
		$("[id^=walkcontEvent-]").click(function(){
			
			
			let id = $(this).attr('id');
			let num = id.split("-")[1];
			let contentId = "#walkcont-"+num;
			
			if( $(contentId).is(':empty') ) { 
				
				$(contentId).load("/bandit/walkthrough/lvl_"+num+".html");
			
			}

		});
	}
	
	function walkthroughGenreator(index){
		
		if(index > 30) return;
			
		var cloned_element = $("#accordion-1").clone().get(0);
				
		var counter = index;
		
		var modified_element = $(cloned_element).find("*").each(function(index, element){
			
			
			$(this).children("span").text(counter);
			
			if($(element).attr("id")){
				
				var attr = $(element).attr('id');
				var new_attr = attr.split("-")[0] + "-" + counter;
				$(element).attr('id', new_attr);
			}
			
			if($(element).attr("data-bs-target")){
				
				var attr = $(element).attr('data-bs-target');
				var new_attr = attr.split("-")[0] + "-" + counter;
				$(element).attr('data-bs-target', new_attr);

				
			}
			
			if($(element).attr("data-bs-parent")){
				
				var attr = $(element).attr('data-bs-parent');
				var new_attr = attr.split("-")[0] + "-" + counter;
				$(element).attr('data-bs-parent', new_attr);

				
			}
			
			if($(element).attr("aria-controls")){
				
				$(element).attr('aria-expanded', 'false');
				$(element).addClass("collapsed");

				var attr = $(element).attr('aria-controls');
				var new_attr = attr.split("-")[0] + "-" + counter;
				$(element).attr('aria-controls', new_attr);

			}
			
			if($(element).attr("aria-labelledby")){
				
				var attr = $(element).attr('aria-labelledby');
				var new_attr = attr.split("-")[0] + "-" + counter;
				$(element).attr('aria-labelledby', new_attr);
				$(element).removeClass('show');

			}
			
		}).get(0);
		
		
		$(modified_element).appendTo("#accordion-1");
		
		counter++
				
		walkthroughGenreator(counter)

}


var newheight = $(window).outerHeight(true) - $("#borderedTab").outerHeight(true);

$("#split-content").height($(window).height());
$("#bandit-tab").height(newheight);

var GUAC_WIDTH = $("#display").width();
var GUAC_HEIGHT = $(window).height();

var BANDITguacdClass;
var BANDITguacdInstance;

pagination();
setupBanditMachine();

walkthroughGenreator(2);
walkthroughEventSetter()

